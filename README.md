# whether image is blurred or not

ML assignment R3.2
Getting Started
This repo contains all my work for this assignment . All the coding is done in deepneural network which i learn recently.

Prerequisites
used keras as main library , Anaconda , convulution neural network.

## installing In anaconda command prompt "conda install keras"

## Model
Step1: Convulution layers

Step2: Max_Polling

Step3: Flattening

Step4:Full_Connection

Step5: Image_Augumentation

## Authors
Bhargav Rachagiri- intial work - "Image classification"

## code

from keras.models import Sequential 

from keras.layers import Convolution2D 

from keras.layers import MaxPooling2D 

from keras.layers import Flatten from keras.layers import Dense


classifier = Sequential()


classifier.add(Convolution2D(32, (3, 3), input_shape = (64, 64, 3), activation = 'relu'))


classifier.add(MaxPooling2D(pool_size = (2, 2)))


classifier.add(Convolution2D(32, (3, 3), activation = 'relu')) classifier.add(MaxPooling2D(pool_size = (2, 2)))


classifier.add(Flatten())


classifier.add(Dense(activation = 'relu', units=128)) classifier.add(Dense(activation = 'sigmoid',units=1))


classifier.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])


from keras.preprocessing.image import ImageDataGenerator

train_datagen = ImageDataGenerator(rescale = 1./255, shear_range = 0.2, zoom_range = 0.2, horizontal_flip = True)

test_datagen = ImageDataGenerator(rescale = 1./255)

training_set = train_datagen.flow_from_directory('TrainingSet', target_size = (64, 64), batch_size = 32, class_mode = 'binary')

test_set = test_datagen.flow_from_directory('EvaluationSet', target_size = (64, 64), batch_size = 32, class_mode = 'binary')

classifier.fit_generator(training_set, steps_per_epoch = 1150, epochs = 5, validation_data = test_set, validation_steps= 1480)

## Acknwoledge
In fullconnection if we change activations like "softmax,sigmoid,relu.. output the acuuracy is differr.

After running different types of activation sigmoid is the best one..



